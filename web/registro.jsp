<%-- 
    Document   : registro
    Created on : 7/10/2021, 02:03:25 AM
    Author     : renemartinez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page Registro Martinez</title>
        <!--Icono-->
        <link rel="Icon" type="img/png" href="img/logoicono.png">
        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>
        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>
        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/style.css"/>
        <!--Ventanas emergentes-->
        <link rel="stylesheet" type="text/css" href="css/ventanas.css">
    </head>
    <body>
        <%
            String matriculaAlu=request.getParameter("matri");
            String nombreAlu=request.getParameter("nombre");
            String carreraAlu=request.getParameter("carrera");
            int edadAlu=Integer.parseInt(request.getParameter("edad"));
            String telAlu=request.getParameter("telefono");
            String correoAlu=request.getParameter("correo");
            String fechaAlu=request.getParameter("fecha");
        %>
        
        
          <div class="container">
            <div class="jumbotron">
                <div class="alert alert-info">
                <center>
                    <h3>Datos del alumnos</h3>
                    <hr>
                    <h4>Matricula Alumno: <%= matriculaAlu%></h4>
                    <h4>Nombre Alumno: <%= nombreAlu%></h4>
                    <h4>Carrera Alumno: <%= carreraAlu%></h4>
                    <h4>Edad Alumno: <%= edadAlu%></h4>
                    <h4>Telefono Alumno: <%= telAlu%></h4>
                    <h4>Correo Alumno: <%= correoAlu%></h4>
                    <h4>Fecha Alumno: <%= fechaAlu%></h4>
                </center>
                </div>
            </div>
          </div>
        
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript" src="js/municipios.js"></script>
    	<script type="text/javascript" src="js/select_estados.js"></script>
    </body>
</html>
